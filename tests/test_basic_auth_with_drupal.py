import unittest
import os

import requests
from requests.auth import HTTPBasicAuth


class TestBasicAuth(unittest.TestCase):

    def test_get_institution(self):
        institution_id = '901b3831-3700-4974-a358-ef3e39dc6438'
        url = f"{os.environ['DRUPAL_API_URL']}/de/jsonapi/node/institution/{institution_id}"
        headers = {
            'X-API-Key': os.environ['DRUPAL_API_KEY']
        }
        user = os.environ['DRUPAL_USERNAME']
        password = os.environ['DRUPAL_PASSWORD']

        response = requests.get(url, headers=headers, auth=HTTPBasicAuth(user, password))

        assert response.ok
