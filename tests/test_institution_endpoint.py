from import_api_app import create_app


class TestInstitutionEndpoint:

    def test_get_institution(self):
        app = create_app()
        app.config['TESTING'] = True
        with app.test_client() as client:
            yield client
        response = client.get('/v1/drupal/institution/901b3831-3700-4974-a358-ef3e39dc6438')
        assert response.status_code == 200
