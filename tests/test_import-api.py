import pytest
from import_api_app import create_app


@pytest.fixture
def client():
    app = create_app()
    app.config['TESTING'] = True
    with app.test_client() as client:
        yield client


def test_job_list(client):
    response = client.get('/v1/jobs')
    assert response.status_code == 200


def test_helm_start(client):
    jobs_response = client.get('/v1/jobs')
    jobs_json = jobs_response.get_json()
    number_of_jobs_before = len(jobs_json)

    response = client.post('/v1/helm/123/456/789/text-file-validation/start', json={
        "job-parameters": {
            "appDirectory": "AFZ/AfZ-Becker-Audiovisuals"
        }
    })
    assert response.status_code == 201

    jobs_response = client.get('/v1/jobs')
    jobs_json = jobs_response.get_json()
    number_of_jobs_after = len(jobs_json)

    assert number_of_jobs_after == number_of_jobs_before + 1

    # try to start a second job with same job id
    response = client.post('/v1/helm/123/456/789/text-file-validation/start', json={
        "job-parameters": {
            "appDirectory": "AFZ/AfZ-Becker-Audiovisuals"
        }
    })
    assert response.status_code == 500

    # use invalid chart name
    response = client.post('/v1/helm/123/456/789/TESTTEST/start', json={
        "job-parameters": {
            "appDirectory": "AFZ/AfZ-Becker-Audiovisuals",
        }
    })
    assert response.status_code == 500


def test_get_job(client):
    response = client.get('/v1/job/789/text-file-validation')
    assert response.status_code == 200

    response = client.get('/v1/job/78999999/text-file-validation')
    assert response.status_code == 404


def test_helm_stop(client):
    response = client.delete('/v1/helm/789/text-file-validation/stop')
    assert response.status_code == 200

    response = client.delete('/v1/helm/789/jdskfhjkdshjkewhrjkewrjkewhjkhrjkewhr/stop')
    assert response.status_code == 500

    response = client.get('/v1/helm/789/text-file-validation')
    assert response.status_code == 404


def test_kafka_topics(client):
    response = client.get('/v1/kafka/topics')
    assert response.status_code == 200
    topics = response.get_json()["topics"]
    number_of_topics_before = len(topics)
    response = client.post('/v1/kafka/topics', json={
        "topics": [
            "py-test1",
            "py-test2"
        ]
    })
    assert response.status_code == 200

    response = client.get('/v1/kafka/topics')
    assert response.status_code == 200
    topics = response.get_json()["topics"]
    assert len(topics) == number_of_topics_before + 2

    # recreate the topics
    response = client.post('/v1/kafka/topics', json={
        "topics": [
            "py-test1",
            "py-test2",
            "py-test3"
        ]
    })
    assert response.status_code == 200

    response = client.get('/v1/kafka/topics')
    assert response.status_code == 200
    topics = response.get_json()["topics"]
    # only one new topic created
    assert len(topics) == number_of_topics_before + 3

    assert "py-test1" in topics
    assert "py-test2" in topics
    assert "py-test3" in topics

    # remove topics
    response = client.delete('/v1/kafka/topics', json={
        "topics": [
            "py-test1",
            "py-test2",
            "py-test3"
        ]
    })
    assert response.status_code == 200

    response = client.get('/v1/kafka/topics')
    assert response.status_code == 200
    topics = response.get_json()["topics"]
    assert len(topics) == number_of_topics_before

    assert "py-test1" not in topics


def test_chain_of_jobs(client):
    # There are integration tests Institutions, RecordSets and Import Processes
    # uuid Import Process 8bd75911-3a1a-414d-aeb7-77518aec9ed2
    # uuid Job Text File Validation adf48fb4-16ba-40b2-a207-8ff9d5aac657
    # uuid Job Text File Validation Results 28c5dc3f-a01c-441e-bffc-711511286e8b

    institution_id = "integration-test"
    record_set_id = "integration-test-record-set"
    process_id = "integrationtest00001"

    # 10 records from Record Set ADG-68432

    # first stop jobs if they are still running
    response = client.delete(
        '/v1/job/' + process_id + '/text-file-validation/stop'
    )

    response = client.delete(
        '/v1/job/' + process_id + '/xml-data-transform/stop',
    )

    response = client.delete(
        '/v1/job/' + process_id + '/mapper-service/stop',
    )

    response = client.delete(
        '/v1/job/' + process_id + '/media-linker/stop',
    )

    response = client.delete(
        '/v1/job/' + process_id + '/normalization-service/stop',
    )

    response = client.delete(
        '/v1/job/' + process_id + '/media-metadata-extractor/stop',
    )

    response = client.delete(
        '/v1/job/' + process_id + '/fedora-ingest-service/stop',
    )

    response = client.post(
        '/v1/job/' + institution_id + '/' + record_set_id + '/' +
        process_id + '/text-file-validation/start',
        json={
         "job-parameters": {
             "appDirectory": record_set_id,
             "drupalJobUuid": "adf48fb4-16ba-40b2-a207-8ff9d5aac657",
             "drupalJobLogResultUuid": "eef31a5c-7f73-4cac-b811-8ba948aef453",
             }
         }
    )
    assert response.status_code == 201
    # check the result :
    # https://mb-wf1.memobase.unibas.ch/jsonapi/paragraph/job_log_result/28c5dc3f-a01c-441e-bffc-711511286e8b

    # this cannot work right now, because the xml-data-transform job entity doesn't exist in drupal
    # to do : put good job result uuid
    response = client.post(
        '/v1/job/' + institution_id + '/' + record_set_id + '/' +
        process_id + '/xml-data-transform/start',
        json={
            "job-parameters": {
                "drupalJobUuid": "XXX",
                "drupalJobLogResultUuid": "153880d6-6628-4b3b-b23e-c6616a0b8ebe",
                "xsltFileName": "transform.xslt",
                "recordTag": "record",
                "identifierFieldName": "identifierMain"
            }
        })
    assert response.status_code == 201

    response = client.post(
        '/v1/job/' + institution_id + '/' + record_set_id + '/' +
        process_id + '/mapper-service/start',
        json={
            "job-parameters": {
                "drupalJobUuid": "XXX",
                "drupalJobLogResultUuid": "c03e2d47-66e5-4c3a-8204-8eee5bcb5675",
                "configFileName": "mapping.yml",
                "lastJobName": "xml-data-transform"
            }
        })
    assert response.status_code == 201

    response = client.post(
        '/v1/job/' + institution_id + '/' + record_set_id + '/' +
        process_id + '/media-metadata-extractor/start',
        json={
            "job-parameters": {
                "drupalJobUuid": "XXX",
                "drupalJobLogResultUuid": "d372c60a-2bfb-438c-af1d-056f9c2e0c30",
                "lastJobName": "mapper-service"
            }
        })
    assert response.status_code == 201

    response = client.post(
        '/v1/job/' + institution_id + '/' + record_set_id + '/' +
        process_id + '/normalization-service/start',
        json={
            "job-parameters": {
                "drupalJobUuid": "XXX",
                "drupalJobLogResultUuid": "2a2239ce-fd1f-49e8-a8ff-dde31d1e13c9",
                "hasLocalTransformConfig": False,
                "lastJobName": "media-metadata-extractor"
            }
        })
    assert response.status_code == 201

    response = client.post(
        '/v1/job/' + institution_id + '/' + record_set_id + '/' +
        process_id + '/fedora-ingest-service/start',
        json={
            "job-parameters": {
                "drupalJobUuid": "XXX",
                "drupalJobLogResultUuid": "e161af46-49fa-4be3-bfc8-9fcfa085113d",
                "appDirectory": record_set_id,
            }
        })
    assert response.status_code == 201
