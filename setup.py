from setuptools import setup

setup(
    name='import_api_app',
    packages=['import_api_app'],
    include_package_data=True,
    install_requires=[
        'flask',
    ],
)
