FROM python:3.8

WORKDIR /import_api_app
ADD import_api_app /import_api_app/
ADD requirements.txt /import_api_app/
WORKDIR /import_api_app/kubectl
#install kubectl so that helm can work in the docker container
RUN wget https://storage.googleapis.com/kubernetes-release/release/v1.16.2/bin/linux/amd64/kubectl && chmod +x ./kubectl
ENV PATH /import_api_app/kubectl:$PATH
WORKDIR /import_api_app
#install helm
RUN wget https://get.helm.sh/helm-v3.2.1-linux-amd64.tar.gz && tar -xvzf helm-v3.2.1-linux-amd64.tar.gz && mv linux-amd64 helm
ENV PATH /import_api_app/helm:$PATH
RUN ./pull-charts.sh
RUN pip install -r requirements.txt
WORKDIR /
ADD setup.py /
RUN pip install -e .
ENV FLASK_APP import_api_app
ENV FLASK_ENV production
EXPOSE 5000
ENTRYPOINT ["python"]
CMD ["/import_api_app/main.py"]
