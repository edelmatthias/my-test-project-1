from import_api_app import create_app
import logging
import os

if __name__ == "__main__":
    numeric_level = getattr(logging, os.getenv('LOG_LEVEL').upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError(f'Invalid log level: {os.getenv("LOG_LEVEL")}')
    logging.basicConfig(format='%(levelname)-8s [%(filename)s:%(lineno)d] %(message)s',
                        level=numeric_level)
    app = create_app()
    app.run(host='0.0.0.0', debug=False)
