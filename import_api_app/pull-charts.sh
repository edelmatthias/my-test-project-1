#!/usr/bin/env bash

export HELM_EXPERIMENTAL_OCI=1

GITLAB_REGISTRY="cr.gitlab.switch.ch"

TFV_REGISTRY=cr.gitlab.switch.ch/memoriav/memobase-2020/services/import-process/text-file-validation
TFV_CHART_VERSION="1.2.0-chart"

IPD_REGISTRY=cr.gitlab.switch.ch/memoriav/memobase-2020/services/deletion-components/import-process-delete
IPD_CHART_VERSION="0.3.0-chart"

helm chart pull ${TFV_REGISTRY}:${TFV_CHART_VERSION}
helm chart export ${TFV_REGISTRY}:${TFV_CHART_VERSION} -d charts/

helm chart pull ${IPD_REGISTRY}:${IPD_CHART_VERSION}
helm chart export ${IPD_REGISTRY}:${IPD_CHART_VERSION} -d charts/
