import os
from flask import current_app
from flask_restful import Resource
from helm import Helm
from helpers.Error import ImportApiError


class DeleteService(Resource):

    def delete_record(self, record_id, session_id, dryrun):
        set_values = {
            'sessionId': session_id,
            'deleteObject': 'record',
            'deleteId': record_id
        }
        try:
            self.do_helm_install(set_values, session_id, dryrun)
        except Exception as ex:
            message = str(ex)
            current_app.logger.error(message)
            raise ImportApiError(message)
            return -1
        return 0

    def delete_recordset(self, recordset_id, session_id, dryrun):
        set_values = {
            'sessionId': session_id,
            'deleteObject': 'record-set',
            'deleteId': recordset_id
        }
        try:
            self.do_helm_install(set_values, session_id, dryrun)
        except Exception as ex:
            message = str(ex)
            current_app.logger.error(message)
            raise ImportApiError(message)
            return -1
        return 0

    def delete_institution(self, institution_id, session_id, dryrun):
        set_values = {
            'sessionId': session_id,
            'deleteObject': 'institution',
            'deleteId': institution_id,
        }
        try:
            self.do_helm_install(set_values, session_id, dryrun)
        except Exception as ex:
            message = str(ex)
            current_app.logger.error(message)
            raise ImportApiError(message)
            return -1
        return 0

    @staticmethod
    def do_helm_install(set_values, session_id, dryrun):
        current_app.logger.debug(
            'calling delete service: type=' +
            set_values['deleteObject'] +
            ' / id=' + set_values['deleteId'] +
            ' / session=' + set_values['sessionId'])
        jobArgs = [
                '--'+set_values['deleteObject']+'-filter',
                set_values['deleteId'],
                set_values['sessionId']
        ]
        if dryrun:
            jobArgs.append('--dry-run')
        jobArgs = {'jobArgs': jobArgs}
        helm = Helm()
        response = helm.install(  # noqa: F841
            chart=os.path.join(current_app.root_path, "charts", 'import-process-delete'),
            name='import-process-delete-' + session_id,
            namespace=current_app.config['NAMESPACE'],
            set_values=jobArgs,
            fail_on_err=False
        )
