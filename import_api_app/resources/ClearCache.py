from flask_restful import Resource, current_app
import requests
import traceback

from requests.auth import HTTPBasicAuth


class ClearCache(Resource):

    def __init__(self):
        self.logger = current_app.logger
        self.headers = {
            'X-API-Key': current_app.config['drupal-api-key']
        }
        user = current_app.config['drupal-user']
        password = current_app.config['drupal-password']
        self.auth = HTTPBasicAuth(user, password)
        self.url = current_app.config['drupal-api-url'] + current_app.config['clear-cache-url']

    def get(self):
        """
        Tell drupal to clear the import-process-cache
        ---
        tags:
          - clear drupal cache
        responses:
          200:
            description: Success, the call to drupal could be executed
          500:
            description: Failure tha call to drupal could neo be executed
        """

        try:
            response = requests.get(self.url, headers=self.headers, auth=self.auth)
        except Exception as ex:
            msg = 'Exception while calling ' + self.url + ': ' + str(ex) + '\n' + \
                  traceback.format_exc()
            self.logger.error(msg)
            return {
                       'response': msg,
                   }, 500
        if response.ok:
            self.logger.debug('successfully called ' + self.url)
            return {
                       'content': response.content.decode("utf-8"),
                   }, response.status_code
        else:
            self.logger.error('Clearing Cache Failed')
            self.logger.error('statuscode: ' + str(response.status_code) + ' content: ' +
                              response.content.decode('utf-8'))
            return {
                       'content': response.content.decode("utf-8"),
                   }, response.status_code
