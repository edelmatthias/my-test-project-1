import hashlib
import os

from flasgger import swag_from
from flask_restful import Resource, reqparse, current_app

from import_api_app.helm import Helm
from import_api_app.resources.FetchMappingFile import FetchMappingFile


class ImportProcessStart(Resource):
    # Todo validate requests
    # @swag.validate('job-parameters')
    @swag_from('ImportProcessStart.yml')
    def post(self, institution_id, record_set_id):
        # get parameters of request-body
        parser = reqparse.RequestParser()
        parser.add_argument('job-parameters', type=dict)
        args = parser.parse_args()
        job_parameters = args['job-parameters']
        job_parameters['institutionId'] = institution_id
        job_parameters['recordSetId'] = record_set_id

        if job_parameters is None:
            job_parameters = {}
        if 'drupalJobUuid' not in job_parameters:
            job_parameters['drupalJobUuid'] = 'adf48fb4-16ba-40b2-a207-8ff9d5aac657'
        if 'drupalJobLogResultUuid' not in job_parameters:
            job_parameters['drupalJobLogResultUuid'] = 'eef31a5c-7f73-4cac-b811-8ba948aef453'
        if 'sessionId' not in job_parameters:
            job_parameters['sessionId'] = 'drupal...'
        if 'xmlRecordTag' not in job_parameters:
            job_parameters['xmlRecordTag'] = 'record'
        if 'xmlIdentifierFieldName' not in job_parameters:
            job_parameters['xmlIdentifierFieldName'] = 'id'
        if 'isPublished' not in job_parameters:
            job_parameters['isPublished'] = 'defaultvalue...'

        # start fetch-mapping-file
        FetchMappingFile.fetchMappingFile(
            self, recordset_id=record_set_id, session_id=job_parameters['sessionId']
        )

        # start text-file-validation
        short_session_id = hashlib.sha1(job_parameters['sessionId'].encode("UTF-8")).hexdigest()[
                           :10]
        body = {'drupalJobUuid': job_parameters['drupalJobUuid'],
                'drupalJobLogResultUuid': job_parameters['drupalJobLogResultUuid'],
                'recordSetId': job_parameters['recordSetId'],
                'sessionId': job_parameters['sessionId'], 'shortSessionId': short_session_id,
                'institutionId': job_parameters['institutionId'],
                'isPublished': job_parameters['isPublished'],
                'kafkaConfigs': current_app.config['tfv-kafka-configs'],
                'sftpConfigs': current_app.config['tfv-sftp-configs'],
                'topicName': current_app.config['tfv-topic-name'],
                'reportingTopicName': current_app.config['tfv-reporting-topic-name'],
                'env': current_app.config['env']
                }

        if 'xmlRecordTag' in job_parameters:
            body['xmlRecordTag'] = job_parameters['xmlRecordTag']
        if 'xmlIdentifierFieldName' in job_parameters:
            body['xmlIdentifierFieldName'] = job_parameters['xmlIdentifierFieldName']
        if 'tableSheetIndex' in job_parameters:
            body['tableSheetIndex'] = job_parameters['tableSheetIndex']
        if 'tableHeaderCount' in job_parameters:
            body['tableHeaderCount'] = job_parameters['tableHeaderCount']
        if 'tableHeaderIndex' in job_parameters:
            body['tableHeaderIndex'] = job_parameters['tableHeaderIndex']
        if 'tableIdentifierIndex' in job_parameters:
            body['tableIdentifierIndex'] = job_parameters['tableIdentifierIndex']

        helm = Helm()
        response = helm.install(
            chart=os.path.join(current_app.root_path, "charts", 'text-file-validation'),
            name=short_session_id + '-' + record_set_id + '-validation',
            namespace=current_app.config['NAMESPACE'],
            set_values=body,
            fail_on_err=False
        )

        if response.stderr == '':
            return {'status': 'SUCCESS', 'message': response.stdout}, 200
        else:
            return {'status': 'FAILED', 'message': response.stderr}, 500
