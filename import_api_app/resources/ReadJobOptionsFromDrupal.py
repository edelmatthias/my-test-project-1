from flask_restful import Resource, current_app
import jsonapi_requests


class ReadJobOptionsFromDrupal(Resource):
    def get(self, job_name, job_drupal_uuid):
        """
        Read the options from Drupal based on a job uuid and transform it to camel case
        ---
        tags:
          - Drupal
        parameters:
          - in: path
            type: string
            name: job_name
            default: table-data-transform
            enum: ['text-file-validation', 'table-data-transform', 'xml-data-transform',
            'mapper-service','media-linker', 'media-metadata-extractor', 'normalization-service',
            'fedora-ingest-service']
            required: true
          - in: path
            type: string
            name: job_drupal_uuid
            description: The drupal uuid of the paragraph describing this job
            default: 481790a3-8ba2-4fd7-95b4-c3a17f08d7d1
        responses:
          200:
            description: The job parameters
        """

        # todo use more standard requests library
        #  (as in the write endpoint) instead of jsonapi_requests
        api = jsonapi_requests.Api.config({
            'API_ROOT': current_app.config['drupal-api-url'] + '/jsonapi',
            # 'AUTH': ('basic_auth_login', 'basic_auth_password'),
            'VALIDATE_SSL': False,
            'TIMEOUT': 1,
        })

        # in drupal job names have underscores instead of -
        job_name = job_name.replace("-", "_")

        endpoint = api.endpoint(
            'paragraph/job_'+job_name+'/'+job_drupal_uuid
        )
        try:
            response = endpoint.get()
        except Exception:
            current_app.logger.error(
                "It was not possible to read from Drupal API"
            )
            return {
                       'error': 'It was not possible to read from Drupal API'
                   }, 500

        result = {}

        for key, value in response.data.attributes.items():
            if key.startswith('field_'):
                result[to_camel_case(key[6:])] = value
        return result


def to_camel_case(snake_str):
    components = snake_str.split('_')
    # We capitalize the first letter of each component except the first one
    # with the 'title' method and join them together.
    return components[0] + ''.join(x.title() for x in components[1:])
