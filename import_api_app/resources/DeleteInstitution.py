from flask_restful import Resource, request
from resources.DeleteService import DeleteService


class DeleteInstitution(Resource):

    def post(self, session_id, dryrun=False):
        """
        Tells the import process deleter to delete a institution
        ---
        tags:
          - delete service
        parameters:
          - in: path
            name: session_id
            required: true
            description: A session id used to distinguish between different runs.
            example: uuid
            type: string
          - in: path
            name: dryrun
            required: false
            description: True if you want to do a dryrun, otherwise False
            example: True
            type: boolean
            default: True
          - in: body
            name: body
            schema:
              id: institution_id
              properties:
                institution_id:
                  type: string
                  example: sik
                  description: The id of the institution to delete
        responses:
          200:
            description: Success, institution to delete has been found and delete service called
            schema:
              properties:
                status:
                  type: string
                  example: SUCCESS/FAILURE
                  enum: ['SUCCESS', 'FAILURE']

        """

        # get institution_id parameter from request-body
        institution_id = request.json['institution_id']
        return DeleteService.delete_institution(DeleteService, institution_id, session_id, dryrun)
