from flask_restful import Resource, request
from resources.DeleteService import DeleteService


class DeleteRecordset(Resource):

    def post(self, session_id, dryrun=False):
        """
        Tells the import process deleter to delete a recordset
        ---
        tags:
          - delete service
        parameters:
          - in: path
            name: session_id
            required: true
            description: A session id used to distinguish between different runs.
            example: uuid
            type: string
          - in: path
            name: dryrun
            required: false
            description: True if you want to do a dryrun, otherwise False
            example: True
            type: boolean
            default: True
          - in: body
            name: body
            schema:
              id: recordset_id
              properties:
                recordset_id:
                  type: string
                  example: sik-001
                  description: The id of the recordset to delete
        responses:
          200:
            description: Success, recordset to delete has been found and delete service called
            schema:
              properties:
                status:
                  type: string
                  example: SUCCESS/FAILURE
                  enum: ['SUCCESS', 'FAILURE']

        """

        # get recordset_id parameter from request-body
        recordset_id = request.json['recordset_id']
        return DeleteService.delete_recordset(DeleteService, recordset_id, session_id, dryrun)
