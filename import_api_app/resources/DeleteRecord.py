from flask_restful import Resource, request
from resources.DeleteService import DeleteService


class DeleteRecord(Resource):

    def post(self, session_id, dryrun=False):
        """
        Tells the import process deleter to delete a record
        ---
        tags:
          - delete service
        parameters:
          - in: path
            name: session_id
            required: true
            description: A session id used to distinguish between different runs.
            example: uuid
            type: string
          - in: path
            name: dryrun
            required: false
            description: True if you want to do a dryrun, otherwise False
            example: True
            type: boolean
            default: True
          - in: body
            name: body
            schema:
              id: record_id
              properties:
                record_id:
                  type: string
                  example: https://memobase.ch/record/sik-001-12
                  description: The id of the record to delete
        responses:
          200:
            description: Success, record to delete has been found and delete service called
            schema:
              properties:
                status:
                  type: string
                  example: SUCCESS/FAILURE
                  enum: ['SUCCESS', 'FAILURE']

        """

        # get record_id parameter from request-body
        record_id = request.json['record_id']
        return DeleteService.delete_record(DeleteService, record_id, session_id, dryrun)
