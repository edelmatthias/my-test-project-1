from flask_restful import Resource
from flasgger import swag_from

from import_api_app.helpers.Error import ImportApiError
from import_api_app.helpers.Helm import stop


class HelmStop(Resource):
    # Todo validate requests
    # @swag.validate('job-parameters')
    @swag_from('HelmStop.yml')
    def delete(self, process_id, job_name):
        try:
            return stop(process_id, job_name)
        except ImportApiError:
            return {'error': 'Unexpected Helm error'}, 500
