import uuid

from flask_restful import Resource, current_app
from kafka import KafkaProducer
import requests
import json
import traceback
from requests.auth import HTTPBasicAuth


class UpdateInstitution(Resource):

    def __init__(self):
        self.producer = KafkaProducer(bootstrap_servers=current_app.config['kafka-broker-url'],
                                      value_serializer=lambda m: json.dumps(m, ensure_ascii=False)
                                      .encode('utf-8'))

    def get(self, institution_drupal_uuid):
        """
        Update the institution with the given drupal UUID in the backend.
        ---
        tags:
          - Update Institution
        parameters:
          - in: path
            name: institution_drupal_uuid
            required: true
            description: The UUID of the updated institution
            example: 0c4c777c-94f8-45ba-945a-bfe6967d40da
            type: string
        responses:
          200:
            description: Success, the information has been written into
                the kafka topic
            schema:
              properties:
                status:
                  type: string
                  example: SUCCESS/FAILURE
                  enum: ['SUCCESS', 'FAILURE']
                topic_value:
                  type: string/json
                  example: the value written into the topic

        """
        result = ''
        headers = {'X-API-Key': current_app.config['drupal-api-key']}
        user = current_app.config['drupal-user']
        password = current_app.config['drupal-password']
        auth = HTTPBasicAuth(user, password)
        base_url = current_app.config['drupal-api-url']
        institution_path = f'/jsonapi/node/institution/{institution_drupal_uuid}'
        extended_address_path = '/jsonapi/paragraph/extended_address/'
        record_set_path = '/jsonapi/node/record_set'
        de_url = f"{base_url}/de{institution_path}"
        fr_url = f"{base_url}/fr{institution_path}"
        it_url = f"{base_url}/it{institution_path}"
        response_de, status_de = UpdateInstitution.request_institution_data(de_url, headers, auth)
        response_fr, status_fr = UpdateInstitution.request_institution_data(fr_url, headers, auth)
        response_it, status_it = UpdateInstitution.request_institution_data(it_url, headers, auth)
        if status_de + status_fr + status_it != 600:
            return {
                       'de': response_de,
                       'fr': response_fr,
                       'it': response_it
                   }, 500
        institution_data_de = response_de
        institution_data_fr = response_fr
        institution_data_it = response_it

        try:
            addresses = []
            address_paragraphs = institution_data_de['relationships']['field_extended_address']
            for element in address_paragraphs['data']:
                paragraph_url = f'{base_url}{extended_address_path}{element["id"]}'
                drupal_response_address = requests.get(paragraph_url, headers=headers, auth=auth)
                response_data = drupal_response_address.json()['data']
                next_address = response_data['attributes']['field_address']
                if next_address is not None:
                    next_address['coordinates'] = \
                        response_data['attributes']['field_geographical_coordinates']['value']
                    addresses.append(next_address)

            institution_types = []
            paragraph_url = \
                institution_data_de['relationships']['field_institution_types']['links']['related'][
                    'href']  # noqa: E501
            institution_types_data = \
                requests.get(paragraph_url, headers=headers, auth=auth).json()['data']
            for element in institution_types_data:
                institution_types.append(element['attributes']['field_wikidata']['uri'])

            record_set_ids = []
            filter_param = f'?filter[field_institution.id][value]={institution_drupal_uuid}'
            url = f'{base_url}{record_set_path}{filter_param}'
            record_set_data = requests.get(url, headers=headers, auth=auth).json()['data']
            for record_set in record_set_data:
                record_set_ids.append(
                    record_set['attributes']['field_memobase_id']
                )
        except LookupError as ex:
            msg = f'Could not find key ({institution_drupal_uuid}): {ex}.'
            current_app.logger.error(msg)
            return {
                       'status': 'FAILURE',
                       'message': str(ex),
                       'topic_value': result
                   }, 500
        except Exception as ex:
            msg = f'Unknown Exception ({institution_drupal_uuid}): {ex}\n{traceback.format_exc()}'
            current_app.logger.error(msg)
            return {
                       'status': 'FAILURE',
                       'message': str(ex),
                       'topic_value': result
                   }, 500
        result = {
            'type': institution_data_de['type'],
            'status': institution_data_de['attributes']['status'],
            'title_de': institution_data_de['attributes']['title'],
            'title_fr': institution_data_fr['attributes']['title'],
            'title_it': institution_data_it['attributes']['title'],
            'field_address': addresses,
            'field_isil': institution_data_de['attributes']['field_isil'],
            'field_memobase_id': institution_data_de['attributes']['field_memobase_id'],
            'field_old_memobase_id': institution_data_de['attributes']['field_old_memobase_id'],
            'field_email': institution_data_de['attributes']['field_email'],
            'field_website': institution_data_de['attributes']['field_website'],
            'field_wikidata_id': institution_data_de['attributes']['field_wikidata_id']['uri'],
            'field_link_archive_catalog':
                institution_data_de['attributes']['field_link_archive_catalog'],
            'field_text_de': institution_data_de['attributes']['field_text'],
            'field_text_fr': institution_data_fr['attributes']['field_text'],
            'field_text_it': institution_data_it['attributes']['field_text'],
            'field_institution_types': institution_types,
            'field_teaser_color': institution_data_de['attributes']['field_teaser_color'],
            'recordset_ids': record_set_ids,
            'computed_teaser_image_url':
                institution_data_de['attributes']['computed_teaser_image_url'],
            'computed_teaser_color': institution_data_de['attributes']['computed_teaser_color'],
        }
        try:
            producer_topic = current_app.config['topic-drupal-export']
            headers = [
                ('recordSetId', bytes('none', encoding='utf-8')),
                ('sessionId', bytes(str(uuid.uuid4()), encoding='utf-8')),
                ('institutionId', bytes(result.get('field_memobase_id'), encoding='utf-8')),
                ('isPublished', bytes(str(result['status']), encoding='utf-8'))
            ]
            key = bytes(result.get('field_memobase_id'), encoding='utf-8')
            self.producer.send(producer_topic, result, key, headers=headers)
        except Exception as ex:
            msg = f'Unknown Exception ({institution_drupal_uuid}): {ex}. '\
                  f'Check logs for more details.'
            current_app.logger.error(f'{msg}\n{traceback.format_exc()}')
            return {
                       'status': 'FAILURE',
                       'topic_key': result.get('field_memobase_id'),
                       'topic_value': result
                   }, 500

        current_app.logger.debug('success for ' + institution_drupal_uuid)
        return {
                   'status': 'SUCCESS',
                   'topic_key': result.get('field_memobase_id'),
                   'topic_value': result
               }, 200

    @staticmethod
    def request_institution_data(url, headers, auth):
        response = requests.get(url, headers=headers, auth=auth)
        if response.ok:
            try:
                json_data = response.json()
                if 'data' in json_data:
                    return json_data['data'], 200
                else:
                    return {'message': "No key 'data' found in json response: " + json_data}, 500
            except json.decoder.JSONDecodeError:
                return {
                           'message': 'Could not parse response message as JSON: ' + response.text
                       }, 404

        else:
            return {
                       'url': url,
                       'message': response.text
                   }, response.status_code
