from flask_restful import Resource, reqparse

from import_api_app.helpers.Error import ImportApiError
from import_api_app.helpers.KafkaTopics import create_topics, list_topics, delete_topics


class KafkaTopics(Resource):
    def get(self):
        """
        Gets the list of kafka topics in the cluster
        ---
        tags:
          - Kafka Topics
        responses:
          200:
            description: Topic list
        """
        try:
            topics = list_topics()
        except ImportApiError as e:
            return {'error': e.message}, 500

        return {
            "topics": list(topics)
        }

    def post(self):
        """
        Creates the Topics from the topics parameter. \
        If the topic already exists it is deleted and recreated
        ---
        tags:
          - Kafka Topics
        parameters:
          - in: body
            name: body
            schema:
              properties:
                topics:
                  type: array
                  example: [test1, test2]
        responses:
          200:
            description: Topic list
        """

        parser = reqparse.RequestParser()
        parser.add_argument('topics', action='append')
        args = parser.parse_args()
        topics = args['topics']

        try:
            return create_topics(topics)
        except ImportApiError as e:
            return {'error': e.message}, 500

    def delete(self):
        """
        Delete the Topics from the topics parameter.
        ---
        tags:
          - Kafka Topics
        parameters:
          - in: body
            name: body
            schema:
              properties:
                topics:
                  type: array
                  example: [test1, test2]
        responses:
          200:
            description: Topic list
        """
        parser = reqparse.RequestParser()
        parser.add_argument('topics', action='append')
        args = parser.parse_args()
        topics = args['topics']

        try:
            return delete_topics(topics)
        except ImportApiError as e:
            return {'error': e.message}, 500
