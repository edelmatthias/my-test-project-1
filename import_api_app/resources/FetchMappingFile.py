from flask_restful import Resource, current_app
from kafka import KafkaProducer

from helpers.Error import ImportApiError
import paramiko


class FetchMappingFile(Resource):

    def get(self, recordset_id, session_id):
        """
        Fetches the mapping file form the sftp-server
        ---
        tags:
          - fetch mapping file
        parameters:
          - in: path
            name: recordset_id
            required: true
            description: The name of the record set (matches folder name on sftp)
            example: AfZ-Becker-Audiovisuals
            type: string
          - in: path
            name: session_id
            required: true
            description: A session id used to distinguish between different runs.
            example: uuid
            type: string
        responses:
          200:
            description: Success, the mapping file has been retrieved
            schema:
              properties:
                status:
                  type: string
                  example: SUCCESS/FAILURE
                  enum: ['SUCCESS', 'FAILURE']
                contents:
                  type: string/yml
                  example: the contents of the mapping file...

        """

        return self.fetchMappingFile(recordset_id, session_id)

    def fetchMappingFile(self, recordset_id, session_id):
        # 1. read file contents from sftp:
        host = current_app.config["sftp_host"]
        port = current_app.config["sftp_port"]
        user = current_app.config["sftp_user"]
        pwd = current_app.config["sftp_password"]
        fileContentsMapping = ''
        fileContentsTransform = ''
        fileContentsLocalTransform = ''

        try:
            ssh_client = paramiko.SSHClient()
            ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            ssh_client.connect(hostname=host,
                               port=port,
                               username=user,
                               password=pwd)
            sftp_client = ssh_client.open_sftp()

            for fileName in ['mapping.yml', 'transform.xslt', 'localTransforms.yml']:
                envDir = '/'
                if current_app.config['env'] == 'stage':
                    envDir = '/stage/'
                elif current_app.config['env'] == 'test':
                    envDir = '/test/'

                remote_path = '/swissbib_index/mb_sftp' + envDir + recordset_id +\
                              '/config/' + fileName
                try:
                    sftp_client.stat(remote_path)
                    remote_file = sftp_client.file(remote_path, 'r')
                    if fileName == 'mapping.yml':
                        fileContentsMapping = remote_file.read()
                    if fileName == 'transform.xslt':
                        fileContentsTransform = remote_file.read()
                    if fileName == 'localTransforms.yml':
                        fileContentsLocalTransform = remote_file.read()
                except IOError:
                    message = 'remote path "' + remote_path + '" does not exist'
                    current_app.logger.debug(message)
            sftp_client.close()
            ssh_client.close()
        except Exception as ex:
            message = str(ex)
            current_app.logger.debug(message)
            raise ImportApiError(message)

        # 2. write file content into kafka topic
        topic = current_app.config['topic-configs']
        try:
            producer = KafkaProducer(bootstrap_servers=current_app.config['kafka-broker-url'],
                                     key_serializer=str.encode)
            key = recordset_id + '#'
            producer.send(topic, key=key + 'mapping', value=fileContentsMapping)
            if "" != fileContentsTransform:
                producer.send(topic, key=key + 'transform', value=fileContentsTransform)
            if "" != fileContentsLocalTransform:
                producer.send(topic, key=key + 'localTransform', value=fileContentsLocalTransform)
        except Exception as ex:
            message = str(ex)
            current_app.logger.debug(message)
            raise ImportApiError(message)
        return {'status': 'SUCCESS'}, 200
