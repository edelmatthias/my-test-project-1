from flask_restful import Resource, reqparse
from flasgger import swag_from

from import_api_app.helpers.Error import ImportApiError
from import_api_app.helpers.Helm import start


class HelmStart(Resource):
    @swag_from('HelmStart.yml')
    def post(self, institution_id, record_set_id, process_id, job_name):

        parser = reqparse.RequestParser()
        parser.add_argument('job-parameters', type=dict)
        args = parser.parse_args()
        job_parameters = args['job-parameters']
        job_parameters['institutionId'] = institution_id
        job_parameters['recordSetId'] = record_set_id
        job_parameters['processId'] = process_id

        try:
            return start(process_id, job_name, job_parameters)
        except ImportApiError as e:
            return {'error': e.message}, 500
