from datetime import timezone


def generate_helm_name(process_id, record_set_id, job_name):
    return record_set_id + '-' + process_id + '-' + job_name


def get_job_info(job):
    """"
    job is a Kubernetes V1Job
    """
    utc_start_time = job.status.start_time
    local_start_time = utc_start_time.replace(tzinfo=timezone.utc).astimezone(tz=None)
    status = 'Unknown'
    if job.status.failed:
        status = 'Failed'
    elif (job.status.active):
        status = 'Running'
    return {
            "status": status,
            "started": local_start_time.strftime("%m/%d/%Y, %H:%M:%S")
    }
