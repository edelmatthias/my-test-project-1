from flask import Flask, send_from_directory, redirect
from kubernetes import config
from flask_restful import Api
from flasgger import Swagger
from kubernetes.config import ConfigException

from import_api_app.resources.WriteJobResultsToDrupal import WriteJobResultToDrupal
from import_api_app.resources.WriteTypeReportToDrupal import WriteTypeReportToDrupal
from import_api_app.resources.UpdateInstitution import UpdateInstitution
from import_api_app.resources.ClearCache import ClearCache
from import_api_app.resources.UpdateRecordSet import UpdateRecordSet
from import_api_app.resources.ImportProcessStart import ImportProcessStart
from import_api_app.resources.FetchMappingFile import FetchMappingFile
from import_api_app.resources.DeleteRecord import DeleteRecord
from import_api_app.resources.DeleteRecordset import DeleteRecordset
from import_api_app.resources.DeleteInstitution import DeleteInstitution
from import_api_app.helpers.Error import ImportApiError
import import_api_app.configuration
import os


def create_app():
    app = Flask(__name__)
    app.config.from_object(import_api_app.configuration)

    # Based on this example :
    # https://github.com/flasgger/flasgger/blob/master/examples/restful.py

    api = Api(app)

    try:
        app.config['import-api-url'] = os.environ['IMPORT_API_URL']
        app.config['kafka-broker-url'] = os.environ['KAFKA_BOOTSTRAP_SERVERS']
        app.config['drupal-user'] = os.environ['DRUPAL_USERNAME']
        app.config['drupal-password'] = os.environ['DRUPAL_PASSWORD']
        app.config['drupal-api-url'] = os.environ['DRUPAL_API_URL']
        app.config['drupal-api-key'] = os.environ['DRUPAL_API_KEY']
        app.config['clear-cache-url'] = os.environ['CLEAR_CACHE_URL']
        app.config['topic-configs'] = os.environ['TOPIC_CONFIGS']
        app.config['topic-drupal-export'] = os.environ['TOPIC_DRUPAL_EXPORT']
        app.config['sftp_host'] = os.environ['SFTP_HOST']
        app.config['sftp_port'] = os.environ['SFTP_PORT']
        app.config['sftp_user'] = os.environ['SFTP_USER']
        app.config['sftp_password'] = os.environ['SFTP_PASSWORD']
        app.config['tfv-kafka-configs'] = os.environ['TFV_KAFKA_SERVER_CONFIGS']
        app.config['tfv-sftp-configs'] = os.environ['TFV_SFTP_CONFIGS']
        app.config['tfv-topic-name'] = os.environ['TFV_TOPIC_NAME']
        app.config['tfv-reporting-topic-name'] = os.environ['TFV_REPORTING_TOPIC_NAME']
        app.config['env'] = os.environ['ENV']

        app.config['SWAGGER'] = {
            'title': 'Memobase Import API',
            'version': 'dev',
            'uiversion': 3,
            'termsOfService': 'http://memobase.ch/de/disclaimer',
            'description': 'API to start, stop, manage import processes for '
                           'memobase. Will be used in the Admin Interface (Drupal).',
            'contact': {
                'name': 'UB Basel',
                'url': 'https://ub.unibas.ch',
                'email': 'swissbib-ub@unibas.ch'},
            'favicon': '/favicon.ico'}
        Swagger(app)
    except Exception as ex:
        raise ImportApiError(str(ex))

    @app.route("/")
    def home():
        return redirect("/apidocs")

    @app.route('/favicon.ico')
    def favicon():
        return send_from_directory(
            os.path.join(
                app.root_path,
                'assets'),
            'favicon.ico',
            mimetype='image/vnd.microsoft.icon')

    api.add_resource(
        ImportProcessStart,
        '/v1/importprocess/<institution_id>/<record_set_id>/start'
    )

    api.add_resource(
        FetchMappingFile,
        '/v1/FetchMappingFile/<recordset_id>/<session_id>'
    )
    api.add_resource(WriteJobResultToDrupal, '/v1/drupal/<job_drupal_uuid>/<job_log_drupal_uuid>')
    api.add_resource(WriteTypeReportToDrupal, '/v1/drupal/WriteElementReport')
    api.add_resource(UpdateInstitution, '/v1/drupal/institution/<institution_drupal_uuid>')
    api.add_resource(UpdateRecordSet, '/v1/drupal/recordset/<record_set_drupal_uuid>')
    api.add_resource(
        DeleteRecord,
        '/v1/drupal/delete/record/<session_id>',
        '/v1/drupal/delete/record/<session_id>/<dryrun>'
    )
    api.add_resource(
        DeleteRecordset,
        '/v1/drupal/delete/recordset/<session_id>',
        '/v1/drupal/delete/recordset/<session_id>/<dryrun>'
    )
    api.add_resource(
        DeleteInstitution,
        '/v1/drupal/delete/institution/<session_id>',
        '/v1/drupal/delete/institution/<session_id>/<dryrun>'
    )
    api.add_resource(ClearCache, '/v1/drupal/clearcache')

    # TODO : maybe take that to a configuration (development vs pod running in
    # k8s cluster)
    try:
        # to be used when inside a kubernetes cluster
        config.load_incluster_config()
    except ConfigException:
        try:
            # use .kube directory
            # for local development
            config.load_kube_config()
        except ConfigException:
            app.logger.error("No kubernetes cluster defined")

    return app
