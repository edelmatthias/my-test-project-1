from flask_restful import current_app

from import_api_app.utility import generate_helm_name
import import_api_app.helm as helm
import os
from subprocess import CalledProcessError
from import_api_app.helpers.Error import ImportApiError


def start(process_id, job_name, job_parameters):
    helm_client = helm.Helm()
    helm_name = generate_helm_name(process_id, job_parameters['recordSetId'], job_name)

    try:
        output = helm_client.install(
            chart=os.path.join(current_app.root_path, "charts", job_name),
            name=helm_name,
            namespace=current_app.config['NAMESPACE'],
            set_values=job_parameters,
            fail_on_err=False
        )
        message = (output.stdout + output.stderr)
        message = message.replace('\n', '. ')
        if output.returncode == 0:
            return {
                       'status': 'SUCCESS',
                       'message': 'Job successfully started',
                       'job_id': helm_name
                   }, 201
        else:
            return {
                       'status': 'FAILURE',
                       'message': message,
                       'job_id': helm_name
                   }, 500
    except CalledProcessError:
        message = "It was not possible to run the helm install command"
        current_app.logger.error(message)
        raise ImportApiError(message)


def stop(process_id, job_name):
    helm_client = helm.Helm()
    helm_name = generate_helm_name(process_id, job_name)

    try:
        output = helm_client.uninstall(
            name=helm_name,
            namespace=current_app.config['NAMESPACE'],
            fail_on_err=False
        )
        message = output.stdout + output.stderr
        message = message.replace('\n', '. ')
        if output.returncode == 0:
            return {
                       'status': 'SUCCESS',
                       'message': message,
                       'job_id': helm_name
                   }, 200
        else:
            return {
                       'status': 'FAILURE',
                       'message': message,
                       'job_id': helm_name
                   }, 500
    except CalledProcessError:
        message = "It was not possible to run the helm uninstall command"
        current_app.logger.error(message)
        raise ImportApiError(message)
