from import_api_app.helpers.Error import ImportApiError
from flask_restful import current_app
from datetime import datetime
import requests
import json


def write_results(job_drupal_uuid, job_log_drupal_uuid, status, report):
    current_app.logger.debug("job-uuid to write: " + job_drupal_uuid)
    current_app.logger.debug("job-report-uuid to write: " + job_log_drupal_uuid)
    current_app.logger.debug("report to write: " + report)
    headers = {
        'Content-Type': 'application/vnd.api+json',
        'Accept': 'application/vnd.api+json',
        'X-API-Key': current_app.config['drupal-api-key']
    }

    calls = {}
    url = current_app.config['drupal-api-url'] + \
        '/jsonapi/node/import_process/' + job_drupal_uuid

    data = {
        "data": {
            "id": job_drupal_uuid,
            "type": "node--import_process",
            "attributes": {
                "field_state": 0
            }
        }
    }
    calls[url] = data

    url = current_app.config['drupal-api-url'] + \
        '/jsonapi/node/log_result/' + job_log_drupal_uuid

    data = {
        "data": {
            "id": job_log_drupal_uuid,
            "type": "node--job_result",
            "attributes": {
                "field_end_date": datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S+00:00"),
                "field_status": status,
                "field_message": report
            }
        }
    }
    calls[url] = data
    returnVal = {'message': ''}

    for call in calls:
        url = call
        data = calls[call]
        try:
            response = requests.patch(
                url,
                headers=headers,
                data=json.dumps(data)
            )
        except requests.exceptions.RequestException:
            message = "It was not possible to write to Drupal API \
                      via the following url " + url
            current_app.logger.error(message)
            raise ImportApiError(message)
        if response.status_code == 200:
            current_app.logger.debug('Updated: '+url)
            returnVal['message'] += 'Updated: ' + url + '\n'
        elif response.status_code == 403:
            message = "Not authorized to write to: " + url
            current_app.logger.error(message)
            raise ImportApiError(message)
        elif response.status_code == 404:
            message = 'Not Found: ' + url
            current_app.logger.error(message)
            raise ImportApiError(message)
        else:
            message = "Unknown response status code for drupal api for url " + url
            current_app.logger.error(message)
            raise ImportApiError(message)
    return returnVal


# make the job as not active any more and write the end data
# todo refactor this in a more generic function
def update_status(job_drupal_uuid, job_name):
    # in drupal job names have underscore instead of minus
    job_name = job_name.replace("-", "_")
    current_app.logger.debug("uuid to write: " + job_drupal_uuid)
    headers = {
        'Content-Type': 'application/vnd.api+json',
        'Accept': 'application/vnd.api+json',
        'X-API-Key': current_app.config['drupal-api-key']
    }

    data = {
        "data": {
            "id": job_drupal_uuid,
            "type": "paragraph--job_"+job_name,
            "attributes": {
                "field_is_active": False
            }
        }
    }

    url = current_app.config['drupal-api-url'] + \
        '/jsonapi/paragraph/job_'+job_name+'/' + job_drupal_uuid

    try:
        response = requests.patch(
            url,
            headers=headers,
            data=json.dumps(data)
        )
    except requests.exceptions.RequestException:
        message = "It was not possible to write to Drupal API \
                  via the following url " + url
        current_app.logger.error(message)
        raise ImportApiError(message)
    if response.status_code == 200:
        current_app.logger.error('Updated: '+url)
        return {'message': 'Updated: ' + url}
    elif response.status_code == 403:
        message = "Not authorized to write to: " + url
        current_app.logger.error(message)
        raise ImportApiError(message)
    elif response.status_code == 404:
        message = 'Not Found: ' + url
        current_app.logger.error(message)
        raise ImportApiError(message)
    else:
        message = "Unknown response status code for drupal api for url " + url
        current_app.logger.error(message)
        raise ImportApiError(message)
