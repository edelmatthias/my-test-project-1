from flask_restful import current_app
from kafka import KafkaConsumer
from kafka.errors import KafkaError
import json

from import_api_app.helpers.Error import ImportApiError


def get_report(topic_name):
    try:  # read from kafka topic and generate report from it:
        consumer = KafkaConsumer(
            topic_name,
            value_deserializer=lambda m: json.loads(m.decode('utf8')),
            bootstrap_servers=current_app.config['kafka-broker-url'],
            auto_offset_reset='earliest',
            enable_auto_commit=False,
            # we stop reading messages after 2 seconds
            consumer_timeout_ms=2000
            # todo maybe add group id ?
            # group_id='import-api-report'+topic_name
        )
    except KafkaError:
        message = 'It was not possible to connect to the Kafka broker'
        current_app.logger.error(message)
        raise ImportApiError(message)

    try:  # read job-reporting messages from kafka
        processed = succeeded = 0
        report = ''
        for jobMessage in consumer:
            msg_value = jobMessage.value
            if msg_value['status'] == 'FAILURE':
                report = report + msg_value['id'] + ": " + msg_value['message'] + "\r\n"
            else:
                succeeded = succeeded + 1
            processed = processed + 1
        report = report[:-2]
        if processed == 0:
            report = 'No messages found'
    except KafkaError:
        message = 'It was not possible to consume the Kafka messages'
        current_app.logger.error(message)
        raise ImportApiError(message)
    # todo: fix this problem, seems to be a problem when there is only
    #  one message in the topic somehow
    except UnboundLocalError:
        if 'text-file-validation' in topic_name:
            report = 'File correctly loaded (this needs to be checked more carefully)'
            current_app.logger.debug("Report: " + report)
            return {
                'status': 'SUCCESS',
                'report': report
            }
        else:
            message = 'Problem generating the report'
            current_app.logger.error(message)
            raise ImportApiError(message)

    if report == '':
        report = str(processed)+' records successfully processed'
        current_app.logger.debug("Report: "+report)
        return {
                   'status': 'SUCCESS',
                   'report': report
               }
    else:
        if 'text-file-validation' in topic_name:
            report = 'Impossible to load the file(s) from the SFTP server'
            current_app.logger.debug("Report: " + report)
            return {
                'status': 'SUCCESS',
                'report': report
            }
        else:
            report_header = str(processed) + ' records processed\r\n'
            report_header += str(succeeded) + ' successful\r\n'
            current_app.logger.debug("Report: " + report_header+report)
            return {
                       'status': 'FAILURE',
                       'report': report_header+report
                   }
