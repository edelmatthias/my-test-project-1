# Import Api App

API to start, stop, manage import processes for memobase. Will be used in the Admin Interface (Drupal). 

Check swagger for documentation of implemented endpoints.